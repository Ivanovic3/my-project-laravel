<?php

namespace Database\Factories;

use App\Models\Users;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsersFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Users::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
           'username'=>$this->faker->userName,
            'profile_picture'=>'profile/BjF5V439YtLGUOsMDy3nFh6SF7FLx1wz7uGft2MY.jpg',
            'email'=>$this->faker->email,
            'password'=>$this->faker->password(8),
            'mobile_number'=>$this->faker->phoneNumber,
            'fb_id'=>$this->faker->randomNumber(8),
            'user_role'=>$this->faker->randomElement(['admin', 'author', 'suscriptor']),
            'access_token'=>$this->faker->password(15),
            'device_token'=>$this->faker->password(15)
        ];
    }
}
