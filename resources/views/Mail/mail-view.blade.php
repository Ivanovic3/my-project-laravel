@extends('templates.template-base')
@section('sub-menu')
    <h2>Mails</h2>
    <ol class="breadcrumb">
       
        <li class="active">
            <strong> Send Email</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                
                <h2>
                   SEND MAIL
                </h2>
                @if(Session::has('status'))
                <div class="alert alert-success alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <a class="alert-link" href="#">{{Session::get('status')}} </a> .
                </div>  
                @endif  
            </div>
            <div class="mail-box">
                <div class="mail-body">
                    <form class="form-horizontal" method="Post" action="mails" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group"><label class="col-sm-2 control-label">To:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" value="alex.smith@corporat.com" name="to"></div>
                        </div>
                        <div class="form-group"><label class="col-sm-2 control-label">Subject:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" value="" name="subject"></div>
                        </div>   
                </div>
                <div class="mail-text h-200">
                    <textarea name="mensaje" id=""  class="summernote" >                       
                                        
                    </textarea>
                    <div class="clearfix"></div>
                </div>
                <div class="mail-body text-right tooltip-demo">
                    <button type="submit" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Send"><i class="fa fa-reply"></i> Send</button>
                </form>
                    </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>   
@endsection