<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActivity extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('activity', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->timestamp('create_date_time')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_date_time')->default(\DB::raw('CURRENT_TIMESTAMP'));
            $table->unsignedBigInteger('user_id');
            $table->string('type');
            $table->string('device_model');
            $table->string('device_type');
            $table->string('operating_system');
            $table->float('latitude');
            $table->float('longitude');
            $table->string('description');
            $table->unsignedBigInteger('artist_id');
            $table->unsignedBigInteger('song_id');
            $table->unsignedBigInteger('album_id');           
            $table->unsignedBigInteger('playlist_id');   
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('activity');
    }
}
