<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('id')->unique();
            $table->timestamp('create_date_time')->default(\DB::raw('CURRENT_TIMESTAMP'))->nullable();
            $table->timestamp('updated_date_time')->default(\DB::raw('CURRENT_TIMESTAMP'))->nullable();
            $table->string('username');
            $table->string('profile_picture')->nullable();
            $table->string('email');
            $table->string('mobile_number')->nullable();
            $table->string('password');
            $table->string('fb_id')->nullable();
            $table->string('user_role')->nullable();
            $table->string('access_token')->nullable();
            $table->string('device_token')->nullable();
            $table->string('api_token')->unique()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
