@extends('templates.template-base')
@section('sub-menu')
    <h2>tu cuenta esta con el id {{$user_auth}}</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html"> List General of Song</a>
        </li>
        <li>
            <a>List Song of : {{ $user_selected }} </a>
        </li>
        <li class="active">
            <strong>Songs</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List of all song</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="col-sm-3">
                        <form action="/search/{{$user_selected}}" method="POST" role="search">
                            @csrf
                            <div class="input-group">
                                <input type="text" placeholder="Search" class="input-sm form-control" name="search"> 
                                <span class="input-group-btn"><button type="submit" class="btn btn-sm btn-primary"> Go!</button> </span>
                            </div>
                        </form>
                    </div>
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                        <tr>
                            <th data-toggle="true">#</th>
                            <th >Name</th>
                            <th>Date Creation</th>
                            <th>Date update</th>
                            <th>Action</th>
                           
                        </tr>
                        </thead>
                        <tbody>
                          
                                @foreach ($songs  as $song)
                                                         
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $song->name}}</td>
                                    <td>{{ $song->year}}</td>
                                    <td>{{ $song->create_date_time}}</td>
                                    <td>
                                        @if ($user_selected==$user_auth)
                                            <form method="POST" action="../user_songs" >
                                                @csrf
                                            
                                                <input type="hidden" value="{{ $song->id }}" name="id-song">    
                                                <button type="submit"  class="fa fa-plus-square" style="color: green;margin-top:10px;border:none;background-color: transparent;">add music</button>
                                            </form>
                                        @else
                                            <form method="POST" action="../users/{{ $user_selected }}/song">
                                                @csrf
                                            
                                                <input type="hidden" value="{{ $song->id }}" name="id-song">
                                                <button type="submit"  class="fa fa-plus-square" style="color: green;margin-top:10px;border:none;background-color: transparent;">add music</button>
                                            </form>
                                        @endif
                                        </td>                                   
                                </tr>
                            @endforeach
                        </tbody>
                       <tfoot style="display: none">
                        <tr>
                            
                            <td colspan="5">
                                
                                <ul class="pagination pull-right" ></ul>
                            </td>
                        </tr> 
                       
                        </tfoot> 
                        <div>
                            
                        </div>
                        
                    </table>
                    {{ $songs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>    
@endsection