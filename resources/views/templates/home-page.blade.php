<html lang="en">
@include('templates.head')
<body>
    <div id="wrapper">
        @include('templates.nav-template')

        <div id="page-wrapper" class="gray-bg">
            @include('templates.menu')
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    @yield('sub-menu')
                <div class="col-lg-2">
            
                </div>
            </div>
            
            
            @yield('content')
            
            @include('templates.footer')
         </div> 
    </div>
@include('templates.scripts')
</body>
</html>



