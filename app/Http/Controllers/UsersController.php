<?php

namespace App\Http\Controllers;

use App\Models\users;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use League\CommonMark\Extension\Attributes\Node\Attributes;
use App\loadImage;
use App\LoadImage\Img;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        
        $users=users::paginate(5);
        $user_auth=auth()->user()->id;
        
        return view('users/users-view')->with('users',$users)->with('user_auth',$user_auth);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
   /* public function create()
    {
        //
    }
*/
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $user=users::find($id);
        $user_auth=auth()->user()->id;
        return view('users/user-edit')->with('user',$user)->with('user_auth',$user_auth);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|string',
            'profile_picture' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|min:8',
            'mobile_number' => 'required|min:7|max:9',
            'rol_user' => 'required|alpha',
        ],['email.unique'=>'el email '.$request->get('email').' ya ah sido registrado']);
        //update user
        $user=users::find($id);
        $user->username=$request->get('name');
        Img::update($request,'profile_picture',$user);
        $user->email=$request->get('email');
        $user->password=Hash::make($request->get('password'));
        $user->mobile_number=$request->get('mobile_number');
        $user->user_role=$request->get('rol_user');
        $user->save();
        $request->session()->flash('update', "$user->username");
        return redirect('users');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user=users::find($id);
        Storage::delete('public/',$user->profile_picture);
        $user->delete();
        return redirect('./users')->with('delete',$user->username);
    }

    public function search(Request $request){
        $users=users::searchUsers($request->search)->paginate(5);
        $user_auth=auth()->user()->id;
        return view('users/users-view')->with('users',$users)->with('user_auth',$user_auth);    
    }
}
