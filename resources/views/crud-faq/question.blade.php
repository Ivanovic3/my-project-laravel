@extends('templates.template-base')
@section('sub-menu')
    <h2>F A Q</h2>
    <ol class="breadcrumb">
    
        
        <li class="active">
            <strong>List Faq</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')



<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List of Question</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li> <a href="./faqs/create" > create new Question</a>
                            </li>
                        </ul>
                    </div>
                    @if(Session::has('store'))
                    <div class="alert alert-success alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <a class="alert-link" href="#">{{Session::get('store')}} </a> successfully saved! .
                    </div>                    
                    @endif
                    @if(Session::has('update'))                    
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('update')}} </a> successfully updated! .
                        </div>                   
                    @endif
                    @if(Session::has('delete'))                   
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('delete')}} </a>  deleted correctly! .
                        </div>                                         
                     @endif
                </div>
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <div class="col-sm-3">
                            <form action="/search_faqs" method="POST" role="search">
                                @csrf
                                <div class="input-group">
                                    <input type="text" placeholder="Search" class="input-sm form-control" name="search"> 
                                    <span class="input-group-btn"><button type="submit" class="btn btn-sm btn-primary"> Go!</button> </span>
                                </div>
                            </form>
                        </div>
                        <thead>
                        <tr>
                            <th data-toggle="true">#</th>
                            <th >Answer</th>
                            <th>Question</th>                           
                            <th>Action</th>                           
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($table_faqs as $item_faq)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $item_faq->answer}}</td>
                                    <td>{{ $item_faq->question}}</td>
                                
                                    <td><a href="./faqs/{{ $item_faq->id}}/edit" ><i class="fa fa-pencil-square-o"> update</i></a>
                                        
                                        <form method="POST" action="{{ route('faqs.destroy',$item_faq->id) }}">
                                            @csrf
                                            @method('DELETE')
                                        
                                            <button type="submit"  class="fa fa-trash" style="color: red;margin-top:10px;border:none;background-color: transparent;">delete</button>
                                        </form>                                       
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="display: none">
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $table_faqs->links() }}
                </div>
            </div>
        </div>
    </div>
</div>  


@endsection