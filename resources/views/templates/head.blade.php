<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title> {{ ('INSPINIA | Profile 2')}} </title>
    <link href="{{  asset('css/bootstrap.min.css')  }}" rel="stylesheet">
    <link href="{{  asset('font-awesome/css/font-awesome.css')  }}" rel="stylesheet">
    <link href="{{  asset('css/animate.css')    }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dropzone/basic.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
    <link href="{{  asset('css/plugins/jasny/jasny-bootstrap.min.css')  }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/codemirror/codemirror.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/footable/footable.core.css') }}" rel="stylesheet">
    <link href="{{  asset('css/style.css')  }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/iCheck/custom.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/summernote/summernote.css') }}" rel="stylesheet">
    <link href="{{ asset('css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">
</head>