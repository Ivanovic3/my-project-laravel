<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class users extends Authenticatable
{
    use HasFactory, Notifiable;
    protected $table='users';
    protected $fillable =[
        'username',
        'profile_picture',
        'email',
        'password',
        'mobile_number',
        'fb_id',
        'user_role',
        'access_token',
        'device_token',
    ];
    //use HasFactory;
    public function userSongs()
    {
        return $this->hasMany(UserSong::class,'user_id');
    }
    public function songs(){
        return $this->belongsToMany(Song::class, 'user_song', 'user_id', 'song_id');
    }
    public function scopeSearchSong($query,$search){
        return $this->belongsToMany(Song::searchSong($search), 'user_song', 'user_id', 'song_id');//->where('name', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('year', 'LIKE', "%$search%");
    }
   public function scopeFindEmail($query, $email){
       return $query->where('email','=',$email);
   }
   public function scopeInnerJoinUser($query,$table_join){
    return $query->join($table_join, 'users.id','=',"$table_join.user_id");
   }
   
   public function getNameUserAttribute($value){
    return $this->attributes['username']=strtoupper($value);
   }
   public function getUsernameAttribute($value)
    {
        return ucfirst($value);
    }
    public function scopeSearchUsers($query,$search)
    {
        return $query->where('username', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('email', 'LIKE', "%$search%")->orWhere('mobile_number', 'LIKE', "%$search%")->orWhere('fb_id', 'LIKE', "%$search%")->orWhere('user_role', 'LIKE', "%$search%");
    }
}
