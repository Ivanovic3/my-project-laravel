<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@yield('head')
<body>
    <div id="app">
         @yield('nav')

        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
</html>
