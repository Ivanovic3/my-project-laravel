@extends('templates.template-base')
@section('sub-menu')
    <h2>CREATE TAG</h2>
    <ol class="breadcrumb">
       
        <li class="active">
            <strong>New Tag<</strong>
        </li>
    </ol>
    </div>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
      <div class="ibox float-e-margins">
          <div class="ibox-title">
              <h5>form to Create new Tag<small>Practica de laravel</small></h5>
          </div>
          <div class="ibox-content">
              <form class="form-horizontal" method="POST" action="../tags">
                  @csrf          
                  <div class="form-group">
                      <label class="col-sm-2 control-label">Name Tag</label>
                      <div class="col-sm-10">
                          <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" >
                      </div>
                  </div>
                  @error('name')
                      <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="hr-line-dashed"></div>

                  <div class="form-group">
                      <label class="col-sm-2 control-label">Date Time</label>
                      <div class="col-sm-10 d-flex">
                        <div class="row">
                          <div class="col-md-3">
                            <div class="input-group date">
                              <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                              <input id="date_added" type="text" class="form-control @error('create-date') is-invalid @enderror" value="03/04/2014" name="create-date">
            
                            </div>
                          </div>
                          <div class="col-md-2">
                            <input class="form-control @error('time') is-invalid @enderror" type="time" value="13:45:00" id="example-time-input" name="time">
                          </div>
                        </div>
                      </div>                           
                  </div>
                  @error('create-date')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  @error('time')
                    <div class="alert alert-danger">{{ $message }}</div>
                  @enderror
                  <div class="hr-line-dashed"></div>
                  
                  <div class="form-group">
                      <div class="col-sm-4 col-sm-offset-2">
                          <a href="../tags" class="btn btn-white">Cancel</a>
                          <button class="btn btn-primary" type="submit">Save changes</button>
                      </div>
                  </div>
              </form>
          </div>
      </div>
  </div>
</div>

  @endsection