<?php

namespace Database\Factories;

use App\Models\Song;
use Illuminate\Database\Eloquent\Factories\Factory;

class SongFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Song::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'create_date_time'=>$this->faker->date.' '.$this->faker->time,
            'name'=>$this->faker->lexify('Cancion de ??????'),
            'year'=>$this->faker->year()
        ];
    }
}
