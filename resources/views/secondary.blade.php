@extends('templates.home-page')
@section('sub-menu')
    <h2>Profile</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">Pin Board</a>
        </li>
        <li>
            <a>Extra Pages</a>
        </li>
        <li class="active">
            <strong>Pin Board</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
   @include('templates.content-pin-board')
@endsection