<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserSong extends Model
{
    use HasFactory;
    protected $table='user_song';
    protected $fillable =[
        'user_id',
        'song_id',
    ];
    public function users(){
        return $this->belongsTo(users::class,'user_id');
    }
    public function songs(){
        return $this->belongsTo(users::class,'song_id');
    }
    public function scopeOrderColumn($query,$columna,$by='desc'){
        return $query->orderBy($columna,$by);
    }
    public function scopeSearchUserSongs($query,$search)
    {
        return $query->where('name', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('year', 'LIKE', "%$search%");
    }
}
