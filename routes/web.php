<?php

use App\Http\Controllers\FaqController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/page1', function () {
    return view('principal');
});
Route::get('/page2', function () {
    $user=App\Models\users::find(2);
    return dd($user->username);
});
Route::get('/page3', function () {
    return view('third');
})->middleware('auth');
Route::get('/optionalParam/{optional?}', function ($optional='es OPCIONAL') {
   
    return 'la ruta tiene el parametro NO requerido '.$optional;
})->middleware('auth');

Route::resource('faqs','App\Http\Controllers\FaqController');
Route::post('/search_faqs',[App\Http\Controllers\FaqController::class, 'search']);

Route::resource('tags','App\Http\Controllers\TaqController');
Route::post('/search_tags',[App\Http\Controllers\TaqController::class, 'search']);

Route::resource('artists','App\Http\Controllers\ArtistController');
Route::post('/search_artists',[App\Http\Controllers\ArtistController::class, 'search']);

Route::resource('users','App\Http\Controllers\UsersController');
Route::post('/search_users',[App\Http\Controllers\UsersController::class, 'search']);

Route::get('songs/{id}',[App\Http\Controllers\SongController::class, 'index'])->middleware('auth');
Route::post('/search/{id}',[App\Http\Controllers\SongController::class, 'search']);

Route::resource('user_songs','App\Http\Controllers\UserSongController')->middleware('auth');
Route::post('/search_user_songs',[App\Http\Controllers\UserSongController::class, 'search']);

Route::resource('users/{id}/song','App\Http\Controllers\ListSongController')->middleware('auth');
Route::post('/search_songs/{id}',[App\Http\Controllers\ListSongController::class, 'search']);

Route::resource('mails','App\Http\Controllers\MailController');
Route::get('changes_password','App\Http\Controllers\AccountsController@indexPasswordRequest');
Route::post('reset_password_with_token', 'App\Http\Controllers\AccountsController@validatePasswordRequest');
Route::post('update_password','App\Http\Controllers\AccountsController@resetPassword');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
