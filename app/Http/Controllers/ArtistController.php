<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Artist;
use Illuminate\Support\Facades\Storage;
use App\loadImage\Img;
use App\Models\users;
use App\Models\UserSong;

class ArtistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {      
        $artists = Artist::paginate(5);
        $user_auth = auth()->user()->id;          
        return view('crud-artist.artist-principal')->with('artists',$artists)->with('user_auth',$user_auth);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $user_auth = auth()->user()->id;
        return view('crud-artist.artist-create',['user_auth'=>$user_auth]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {               
        $request->validate([
            'name' => 'required|string',
            'photo' => 'required',
            'country' => 'required|alpha',
            'favorite_count' => 'required|numeric|min:1|max:100',
        ]);
        $fields=$request->all();
        $fields['photo']=Img::add($request,'photo');
        $artist=Artist::create($fields);
        $request->session()->flash('update', "$artist->name");
        return redirect('artists');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $artist_data=Artist::find($id);
        $user_auth=auth()->user()->id;
        return view('crud-artist.artist-edit')->with('artist_data',$artist_data)->with('user_auth',$user_auth);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $request->validate([
            'name' => 'string',
            'country' => 'string',
            'favorite' => 'numeric|min:1|max:100',
        ]);

        $artist = Artist::find($id);
        $fields=$request->all();
        $fields['photo']=Img::update($request,'photo',$artist);
        $artist->update($fields);

        $request->session()->flash('update', "$artist->name");
        return redirect('artists');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $artist=Artist::find($id);
        Storage::delete('public/',$artist->photo);
        $artist->delete();
        return redirect('./artists')->with('delete',$artist->name);
    }
    public function search(Request $request){
        $artists=Artist::searchArtists($request->search)->paginate(5);
        $user_auth=auth()->user()->id;
        return view('crud-artist.artist-principal')->with('artists',$artists)->with('user_auth',$user_auth);
    }
}
