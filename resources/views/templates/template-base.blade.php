<html lang="en">
@include('templates.head')
<body>
    <div id="wrapper" style="background-color: #454d58;">
        @include('templates.menu2-template')

        <div id="page-wrapper" class="gray-bg">
            @include('templates.menu-template')
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    @yield('sub-menu')
                <div class="col-lg-2">
            
                </div>
            </div>
            
            
            @yield('content')
            
          
         </div> 
         
    </div>
@include('templates.scripts')
</body>
</html>



