<nav class="navbar-default navbar-static-side" role="navigation" style="background-color: black;heigth:100%">
    <div class="sidebar-collapse">
        <ul class="nav metismenu" id="side-menu">
            <li class="nav-header">
                <div class="dropdown profile-element"> <span>
                        <img alt="image" class="img-circle" width="100" height="100" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSIX8d_IOIM8LwRjfxegX6m-mTfjgXSGiwJBg&usqp=CAU" />
                         </span>
                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                        <span class="clear"> 
                            <span class="block m-t-xs">
                                 <strong class="font-bold">PROYECT LARAVEL</strong>
                         </span>  
                        </span> 
                    </a>
                   
                </div>
                <div class="logo-element">
                    PL
                </div>
            </li>
        
            <li class="active">
                <a href="#"><i class="fa fa-pencil"></i> <span class="nav-label">Herramientas </span>  <span class="pull-right label label-primary">CRUDS</span></a>
                <ul class="nav nav-second-level">
                    <li><a href="{{ url('tags') }}">TAG</a></li>
                    <li><a href="{{ url('faqs') }}">FAQ</a></li>
                    <li><a href="{{ url('artists') }}">ARTIST</a></li>
                    <li><a href="{{ url('users') }}">USERS</a></li>
                    <li><a href="{{ url("songs/$user_auth") }}">ALL SONGS</a></li>
                    <li><a href="{{ url('user_songs') }}">MY SONGS</a></li>
                    <li><a href="{{ url('mails') }}">SEND MAILS</a></li>
                </ul>
            </li>
           
            
            

           
            
            
            <li class="landing_link">
                <a target="_blank" href="https://www.laravel.com"><i class="fa fa-book"></i> <span class="nav-label">DOC LARAVEL</span> <span class="label label-warning pull-right">DOCS</span></a>
            </li>

        </ul>

    </div>
</nav>
