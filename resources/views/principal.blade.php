@extends('templates.home-page')
@section('sub-menu')
    <h2>Profile</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">Home</a>
        </li>
        <li>
            <a>Extra Pages</a>
        </li>
        <li class="active">
            <strong>Profile</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
   @include('templates.content-template')
@endsection