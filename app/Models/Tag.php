<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    use HasFactory;
   
    protected $table='tag';
    protected $fillable =[
        'name',
        'create_date_time'
    ];
    public function scopeOrderColumn($query,$columna,$by='desc'){
        return $query->orderBy($columna,$by);
    }
    public function scopeSearchTags($query,$search)
    {
        return $query->where('name', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('create_date_time', 'LIKE', "%$search%")->orWhere('updated_date_time', 'LIKE', "%$search%");
    }
}
