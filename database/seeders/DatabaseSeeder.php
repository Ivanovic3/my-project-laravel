<?php

namespace Database\Seeders;

use App\Models\Faq;
use App\Models\User;
use Faker\Factory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {   
        // \App\Models\User::factory(10)->create();
        $this->call([
            FaqSeeder::class,
            TagSeeder::class,
            UsersSeeder::class,
            ArtistSeeder::class,
            SongSeeder::class
        ]);
    }
}
