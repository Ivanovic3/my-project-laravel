@extends('templates.template-base')
@section('sub-menu')
    <h2>Edit Artist</h2>
    <ol class="breadcrumb">
    
        <li class="active">
            <strong>Update Artist</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
  <div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>form to create new artist <small>Practica de laravel</small></h5>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" method="POST" action="../{{ $artist_data->id }}" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $artist_data->name }}">
                        </div>
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                      
                        <label class="col-sm-2 control-label">Photo</label>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="fileinput fileinput-new input-group" data-provides="fileinput">
                                    <div class="form-control" data-trigger="fileinput">
                                        <i class="glyphicon glyphicon-file fileinput-exists"></i>
                                    <span class="fileinput-filename"></span>
                                    </div>
                                    <span class="input-group-addon btn btn-default btn-file">
                                        <span class="fileinput-new">Select file</span>
                                        <span class="fileinput-exists">Change</span>
                                        <input type="file" class="@error('photo') is-invalid @enderror" id="photo" name="photo"/>
                                    </span>
                                    <a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
                                </div> 
                            </div> 
                        </div> 
                    </div> 
                    <div class="text-center">
                      <img src="{{ asset('storage').'/'.$artist_data->photo }}" alt="" width="200" >
                    </div>
                    
                    @error('photo')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Country</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('country') is-invalid @enderror" name="country" value="{{ $artist_data->country }}" >
                        </div>                           
                    </div>
                    @error('country')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Favorite Count</label>
                        <div class="col-sm-10">
                            <input type="number" class="form-control @error('favorite') is-invalid @enderror" name="favorite" value="{{ $artist_data->favorite_count }}">
                        </div>                                                     
                    </div>
                    @error('favorite')
                      <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <a href="../../artists" class="btn btn-white">Cancel</a>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>
  @endsection