@extends('templates.template-base')
@section('sub-menu')
    <h2>New QUESTION</h2>
    <ol class="breadcrumb">
   
        <li class="active">
            <strong>Create question</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>form to create new question <small>Practica de laravel</small></h5>
                </div>
                <div class="ibox-content">
                    <form class="form-horizontal" method="POST" action="../faqs">
                        @csrf
                    
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Answer</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('answer') is-invalid @enderror" name="answer" >
                            </div>
                        </div>
                        @error('answer')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="hr-line-dashed"></div>

                        <div class="form-group">
                            <label class="col-sm-2 control-label">Question</label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control @error('question') is-invalid @enderror" name="question"  >
                            </div>                           
                        </div>
                        @error('question')
                        <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                        <div class="hr-line-dashed"></div>
                        
                        <div class="form-group">
                            <div class="col-sm-4 col-sm-offset-2">
                                <a href="../../faqs" class="btn btn-white">Cancel</a>
                                <button class="btn btn-primary" type="submit">Save changes</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  @endsection