<?php
namespace App\LoadImage;

use App\Models\Artist;
use Illuminate\Database\Eloquent\Model;
use \Illuminate\Http\Request ;
use Illuminate\Support\Facades\Storage;

class Img{
    public static function add(Request $request,$nameAtribute){
        if($nameAtribute==='photo'){
       return $request->file($nameAtribute)->store('uploads','public');
    } 
    else{
        return $request->file($nameAtribute)->store('profile','public');
    }

    }
    public static function update(Request $request,$nameAtribute,Model $model){
            //dd($nameAtribute);
        if ($request->hasFile($nameAtribute)) {
            //dd('public/',$model->$nameAtribute);
            Storage::delete('public/',$model->$nameAtribute);
            if($nameAtribute==='photo'){
            return $model->$nameAtribute=$request->file($nameAtribute)->store('uploads','public');
            } 
            else{
            return $model->$nameAtribute=$request->file($nameAtribute)->store('profile','public');    
            }  
        }
     }
     
}
?>