@extends('templates.template-base')
@section('sub-menu')
    <h2>USERS</h2>
    <ol class="breadcrumb">
        <li>
            <a>List Users</a>
        </li>
        <li class="active">
            <strong>Users</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')

<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List of User</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li> <a href="./register" > create new artist</a>
                            </li>
                        </ul>
                    </div>
                   
                    @if(Session::has('update'))                    
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('update')}} </a> successfully updated! .
                        </div>                   
                    @endif
                    @if(Session::has('delete'))                   
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('delete')}} </a>  deleted correctly! .
                        </div>                                         
                    @endif
                </div>
                <div class="ibox-content">

                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <thead>
                        <tr>
                            <th data-toggle="true">#</th>
                            <th >Username</th>
                            <th>email</th>
                            <th>mobile_number</th>
                            <th>user_role</th> 
                            <th>Action</th>
                            <th data-hide="all">perfile image</th>
                        </tr>
                        </thead>
                        <tbody>
                            <div class="col-sm-3">
                                <form action="/search_users" method="POST" role="search">
                                    @csrf
                                    <div class="input-group">
                                        <input type="text" placeholder="Search" class="input-sm form-control" name="search"> 
                                        <span class="input-group-btn"><button type="submit" class="btn btn-sm btn-primary"> Go!</button> </span>
                                    </div>
                                </form>
                            </div>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $user->username}}</td>
                                    <td>{{ $user->email}}</td>
                                    <td>{{ $user->mobile_number}}</td>
                                    <td>{{ $user->user_role}}</td>
                                    <td><a href="./users/{{ $user->id }}/edit"><i class="fa fa-pencil-square-o"> update</i></a>
                                        <div style="margin-top:10px"><a href="./users/{{ $user->id }}/song" style="color: rgb(34, 155, 94)"><i class="fa fa-eye"> view song</i></a></div>
                                        <form method="POST" action="{{ route('users.destroy',$user->id) }}">
                                            @csrf
                                        @method('DELETE')
                                        
                                            <button type="submit"  class="fa fa-trash" style="color: red;margin-top:10px;border:none;background-color: transparent;">delete</button>
                                        </form>
                                        
                                        </td>
                                    <td><img src="{{ asset('storage').'/'.$user->profile_picture }}" alt="" width="200"></td>
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="display: none">
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $users->links() }}
                </div>
            </div>
        </div>
    </div>
</div>   
    
@endsection