<?php

namespace Database\Seeders;

use App\Models\Faq;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //dd(Faq::factory(1)->create());
        Faq::factory(10)->create();
       
       
        DB::table('faq')->insert([
                
            'answer' =>'respuesta '.Str::random(10),
            'question' =>'pregunta '.Str::random(10),
            'created_at' => date("Y-m-d H:i:s"),
            'updated_at' => date("Y-m-d H:i:s"),    
            
        ]);
       
    }
}
