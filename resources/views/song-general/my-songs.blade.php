@extends('templates.template-base')
@section('sub-menu')
    <h2>MY SONGS</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">My Songs of user : {{ $user_auth->username }}</a>
        </li>
        
        <li class="active">
            <strong> list Songs</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')


<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>List of My song</h5>
                    <div class="ibox-tools">
                        <a class="collapse-link">
                            <i class="fa fa-chevron-up"></i>
                        </a>
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="fa fa-plus"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li> <a href="songs/{{ $user_auth->id }}" > add song your list</a>
                            </li>
                        </ul>
                    </div>
                    @if(Session::has('store'))
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('store')}} </a> successfully saved! .
                        </div>                    
                    @endif
                    @if(Session::has('delete'))                   
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('delete')}} </a>  deleted correctly! .
                        </div>                                         
                    @endif
                </div>
                <div class="ibox-content">
                    <table class="footable table table-stripped toggle-arrow-tiny">
                        <div class="col-sm-3">
                            <form action="/search_user_songs" method="POST" role="search">
                                @csrf
                                <div class="input-group">
                                    <input type="text" placeholder="Search" class="input-sm form-control" name="search"> 
                                    <span class="input-group-btn"><button type="submit" class="btn btn-sm btn-primary"> Go!</button> </span>
                                </div>
                            </form>
                        </div>
                        <thead>
                        <tr>
                            <th data-toggle="true">#</th>
                            <th >Name</th>
                            <th>Date Creation</th>
                            <th>Date update</th>
                            <th>Action</th>
                           
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($songs_of_user as $song)
                                <tr>
                                    <td>{{ $loop->index+1 }}</td>
                                    <td>{{ $song->name}}</td>
                                    <td>{{ $song->year}}</td>
                                    <td>{{ $song->create_date_time}}</td>
                                    <td>
                                        
                                        <form method="POST" action="{{ route('user_songs.destroy',$song->id) }}">
                                            @csrf
                                        @method('DELETE')
                                        
                                            <button type="submit"  class="fa fa-trash" style="color: red;margin-top:10px;border:none;background-color: transparent;">delete</button>
                                        </form>
                                        
                                        </td>
                                    
                                </tr>
                            @endforeach
                        </tbody>
                        <tfoot style="display: none">
                        <tr>
                            <td colspan="5">
                                <ul class="pagination pull-right"></ul>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    {{ $songs_of_user->links() }}
                </div>
            </div>
        </div>
    </div>
</div>  
@endsection