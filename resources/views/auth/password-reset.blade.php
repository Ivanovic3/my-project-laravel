@extends('layouts.app')
@section('head')
@include('templates.head')

@endsection
@section('nav')
    <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'Laravel') }}
            </a>

        </div>
    </nav>   
@endsection
@section('content')
<div class="container">
<div class="wrapper wrapper-content animated fadeInRight">
    <div class="row">
        <div class="col-lg-8">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <h5>Inline form</h5>
                    
                    @if(Session::has('status'))
                        <div class="alert alert-success alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('status')}}</a>.
                        </div>
                    @endif

                    @if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissable">
                            <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                            <a class="alert-link" href="#">{{Session::get('error')}}</a>.
                        </div>
                    @endif
                </div>
                <div class="ibox-content">
                    <form role="form" class="form-inline" method="POST" action="{{ url('/reset_password_with_token') }}">
                       @csrf
                        <div class="form-group">
                            <label for="exampleInputEmail2" class="sr-only">Email address</label>
                            <input type="email" placeholder="Enter email" id="exampleInputEmail2"
                                class="form-control" name="email">
                        </div>

                        @if(Session::has('email'))
                            <div class="alert alert-danger alert-dismissable">
                                <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                                no existe el usuaaro del email :<a class="alert-link" href="#">{{Session::get('email')}}</a>.
                            </div>
                        @endif

                        <div class="form-group" style="margin-top: 6px">
                        <button class="btn btn-white" type="submit">Request</button>
                        <a href="login" class="btn btn-white" type="submit">back Login</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@include('templates.scripts')

@endsection



    
