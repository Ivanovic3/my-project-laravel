<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    use HasFactory;
    protected $table='artist';
    protected $fillable =[
        'name',
        'photo',
        'country',
        'favorite_count'
    ];
    public function scopeOrderColumn($query,$columna,$by='desc'){
        return $query->orderBy($columna,$by);
    }
    public function scopeSearchArtists($query,$search)
    {
        return $query->where('name', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('country', 'LIKE', "%$search%")->orWhere('favorite_count', 'LIKE', "%$search%");
    }
}
