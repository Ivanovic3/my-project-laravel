<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    use HasFactory;
    protected $table='faq';
    protected $fillable =[
        'answer',
        'question'
    ];
    public function scopeOrderColumn($query,$columna,$by='desc'){
        return $query->orderBy($columna,$by);
    }
    public function scopeSearchFaqs($query,$search)
    {
        return $query->where('answer', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('question', 'LIKE', "%$search%");
    }
}
