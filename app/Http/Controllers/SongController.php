<?php

namespace App\Http\Controllers;

use App\Models\Song;
use Illuminate\Http\Request;

class SongController extends Controller
{
    public function index($id)
    {  
        $songs= Song::paginate(5);
        $user_auth=auth()->user()->id;
        return view('song-general.song-view',['songs'=>$songs,'user_selected'=>$id,'user_auth'=>$user_auth]);
    }
    public function search(Request $request,$id){

        $songs=Song::searchSong($request->search)->paginate(5);
        $user_auth=auth()->user()->id;
        return view('song-general.song-view',['songs'=>$songs,'user_selected'=>$id,'user_auth'=>$user_auth]);
        
    }
}
