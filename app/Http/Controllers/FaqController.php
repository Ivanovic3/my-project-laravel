<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Faq;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $table_faqs=Faq::paginate(5);
        $user_auth=auth()->user()->id;
        return view('crud-faq.question')->with('table_faqs',$table_faqs)->with('user_auth',$user_auth);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   $user_auth=auth()->user()->id;
        return view('crud-faq.create-question',['user_auth'=>$user_auth]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   

        $request->validate([
            'answer' => 'required|string|min:10|max:64|nullable',
            'question' => 'required|string|min:10|max:64|nullable',
            
        ]);
        
        $faq=new Faq();
        $faq->answer= $request->get('answer');
        $faq->question= $request->get('question');
        $faq->save();
        $request->session()->flash('update', "$faq->answer");
        return redirect('./faqs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {   
        $faq=Faq::find($id);
        $user_auth=auth()->user()->id;
        
        return view('crud-faq.edit-question')->with('faq',$faq)->with('user_auth',$user_auth);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'answer' => 'required|string|min:10|max:64|nullable',
            'question' => 'required|string|min:10|max:64|nullable',
        ]);
        $faq = Faq::find($id);
        $faq->answer = $request->get('answer');
        $faq->question = $request->get('question');
        $faq->save();
        $request->session()->flash('update', "$faq->answer");
        return redirect('./faqs');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $faq = Faq::find($id);
        $faq->delete();
        return redirect('./faqs')->with('delete',$faq->answer);
    }
    public function search(Request $request){
        $table_faqs=Faq::searchFaqs($request->search)->paginate(5);
        $user_auth=auth()->user()->id;
        return view('crud-faq.question')->with('table_faqs',$table_faqs)->with('user_auth',$user_auth);
        
    }
}
