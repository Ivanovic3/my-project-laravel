<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    use HasFactory;
    protected $table='password_resets';

    public function scopeInsertData($query, $email,$token,$create){
        return $query->insert([
            'email' => $email,
            'token' => $token,
            'created_at' => $create
        ]);
    }
    public function scopeFindEmail($query, $data){
        return $query->where('email', $data);
    }
    public function scopeFindToken($query, $token){
        return $query->where('token',$token);
    }
}
