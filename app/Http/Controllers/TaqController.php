<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Tag;
use DateTime;
use Illuminate\Contracts\Session\Session;

class TaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $tags=Tag::paginate(5);
        $user_auth=auth()->user()->id;
        return view('crud-tag.tag-principal')->with('tags',$tags)->with('user_auth',$user_auth);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {   
        $user_auth=auth()->user()->id;
        return view('crud-tag.tag-create',['user_auth'=>$user_auth]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $request->validate([
            'create-date' => 'required',
            'time' => 'required',
            'name' => 'required|string',
        ]);
        //transform the date a format correct
        $fecha=strtotime(str_replace("/", "-", $request->get('create-date')).$request->get('time'));
        
        $tag=new Tag();
        $tag->name= $request->get('name');
        $tag->create_date_time=date("Y-m-d H:i:s", "$fecha");
        $tag->save();
        $request->session()->flash('store', "$tag->name");
        return redirect('tags');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {           
        $tag=Tag::find($id);
        $user_auth=auth()->user()->id;
        return view('crud-tag.tag-edit')->with( 'tag', $tag)->with('user_auth',$user_auth);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tag=Tag::find($id);
        $request->validate([
            'data-time' => 'required|date_format:Y-m-d H:i:s',
            'name' => 'required|alpha',
        ]); 
        $tag->name= $request->get('name');
        $tag->create_date_time=$request->get('data-time');
        $tag->save();
        $request->session()->flash('update', "$tag->name");
        return redirect('tags');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $tag=Tag::find($id);
        $tag->delete();
        return redirect('tags')->with('delete', "$tag->name");;
    }
    public function search(Request $request){
        $tags=Tag::searchTags($request->search)->paginate(5);
        $user_auth=auth()->user()->id;
        return view('crud-tag.tag-principal')->with('tags',$tags)->with('user_auth',$user_auth); 
    }
}
