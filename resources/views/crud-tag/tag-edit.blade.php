@extends('templates.template-base')
@section('sub-menu')
    <h2>EDIT TAG</h2>
    <ol class="breadcrumb">
    
       
        <li class="active">
            <strong>update TAG</strong>
        </li>
    </ol>
    </div>
@endsection

@section('content')




<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-title">
                <h5>form to Create new Tag<small>Practica de laravel</small></h5>
            </div>
            <div class="ibox-content">
                <form class="form-horizontal" method="POST" action="../{{ $tag->id }}">
                    @csrf
                    @method('PUT')     
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name Tag</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ $tag->name }}">
                        </div>
                    </div>
                    @error('name')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>
  
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Data time</label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control @error('data-time') is-invalid @enderror" name="data-time"  value="{{  $tag->create_date_time }}">
                        </div>
                    </div>
                    @error('data-time')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <div class="hr-line-dashed"></div>
                    
                    <div class="form-group">
                        <div class="col-sm-4 col-sm-offset-2">
                            <a href="../../tags" class="btn btn-white">Cancel</a>
                            <button class="btn btn-primary" type="submit">Save changes</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
  </div>

  @endsection