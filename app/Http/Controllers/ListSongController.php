<?php

namespace App\Http\Controllers;

use App\Models\Song;
use App\Models\users;
use App\Models\UserSong;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\Input;

class ListSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {  
        $user = users::find($id);
        $songs_of_user=users::find($id)->songs()->paginate(5);
        Song::byUser('11')->fromYear(2015)->get();
        $user_auth = auth()->user()->id;

        return view('song-general.song-for-user',['user'=>$user,'songs_of_user'=>$songs_of_user,'user_auth'=>$user_auth]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {      
        $song_of_user = new UserSong;
        $song_of_user->user_id=$id;
        $song_of_user->song_id=$request->get('id-song');
        $song_of_user->save();
        $request->session()->flash('store', " ");
        return redirect("users/$id/song");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id,$song)
    {  
        UserSong::find($song)->delete();
        return redirect("users/$id/song")->with('delete','eliminado correctament');
    }
}
