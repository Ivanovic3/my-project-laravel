<?php

namespace App\Http\Controllers\Api\v1;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\LoadImage\Img;
use App\Models\users;
use Illuminate\Contracts\Validation\Validator as ValidationValidator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationData;
use Illuminate\Validation\Validator as IlluminateValidationValidator;
use Symfony\Component\VarDumper\VarDumper;

class UserController extends Controller
{
    public function login(Request $request){
        $validation= $this->verificationEmail($request);
        if($validation==false){    
            return response()->json(['data'=>'verifica los datos que ingresaste sean correctos'],400);
        }

        if (Auth::attempt($request->all())) {
            $user = Auth::user();
            $this->generateToken($user);
            return response()->json(['data'=>new UserResource($user)]);
        }

        return response()->json(['data'=>'el usuario ingresado no existe'],400);
    }

    public function verificationEmail(Request $request){
        $validator = Validator::make($request->all(),[
            'email'    => 'required|string|email',
            'password' => 'required|string'
            ]);

        if ($validator->fails()) {
            return false;
        }
        return true;
    }

    public function generateToken($user)
    {
        $user->api_token = Str::random(60);
        $user->save();
        return $user->api_token;
    }

    public function update(Request $request)
    {   
        //verifiqued la validacion
        $validation= $this->verificationUser($request);
        if($validation==false){    
            return response()->json(['data'=>'verifica que los datos cumplan con los requisitos'],400);
        }
        //update user
        $user = users::find($request->id);
        $fields=$request->all();
        $fields['profile_picture']=Img::update($request,'profile_picture',$user);
        $user->update($fields);

        return response()->json(['data'=>new UserResource($user)], 200);
    }

    private function verificationUser(Request $request){
        $validator = Validator::make($request->all(),[
            'name' => 'required|string',
            'profile_picture' => 'required',
            'email' => 'required|email',
            'password' => 'required|string|min:8',
            'mobile_number' => 'required|min:7|max:9',
            'rol_user' => 'required|string',
            ]);

        if ($validator->fails()) {
            return false;
        }
        return true;
    }
}
