@extends('templates.home-page')
@section('sub-menu')
    <h2>Profile</h2>
    <ol class="breadcrumb">
        <li>
            <a href="index.html">Project Detail</a>
        </li>
     
        <li class="active">
            <strong>Project Detail</strong>
        </li>
    </ol>
    </div>
@endsection
@section('content')
   @include('templates.content-project')
@endsection