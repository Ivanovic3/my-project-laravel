<?php

namespace App\Http\Controllers;

use App\Models\Song;
use App\Models\users;
use App\Models\UserSong;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class UserSongController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $user_auth = auth()->user();
        $songs_of_user = users::find($user_auth->id)->songs()->paginate(5);
        return view('song-general.my-songs',['songs_of_user'=>$songs_of_user,'user_auth'=>$user_auth]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {   
        $song_of_user = new UserSong;
        $song_of_user->user_id = auth()->user()->id;
        $song_of_user->song_id = $request->get('id-song');
        $song_of_user->save();
        $request->session()->flash('store', "");
        return redirect('user_songs');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $song_of_user = UserSong::find($id);
        $song_of_user->delete();
        return redirect('user_songs')->with('delete',' ' );
    }
    public function search(Request $request){
        $user_auth=auth()->user()->id;
        
        $songs_of_user=users::find($user_auth->id)->songs()->searchUserSongs($request->search)->paginate(5);
        return view('song-general.my-songs',['songs_of_user'=>$songs_of_user,'user_auth'=>$user_auth]);
   }
}
