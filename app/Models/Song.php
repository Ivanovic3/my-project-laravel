<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Song extends Model
{
    use HasFactory;
   use Searchable;

   
    protected $table='song';

    public function scopeSearchSong($query,$search)
    {
        return $query->where('name', 'LIKE', "%$search%")->orWhere('id', 'LIKE', "%$search%")->orWhere('year', 'LIKE', "%$search%");
    }
   

    public function userSongs()
    {
        return $this->hasMany(UserSong::class,'song_id');
    }
    public function users(){
        return $this->belongsToMany(users::class, 'user_song', 'user_id', 'song_id');
    }

    public function scopeFromYear($query,$year)
    {
        return $query->where('year', '>=', $year);
    }
    public function scopeByUser($query, $user_id){
        return $query->join('user_song', 'song.id', '=', 'user_song.song_id')
        ->where('user_song.user_id','=',$user_id);
    }
    public function scopeOrderColumn($query,$columna,$by='desc'){
        return $query->orderBy($columna,$by);
    }  
}
